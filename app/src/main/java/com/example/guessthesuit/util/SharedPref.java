package com.example.guessthesuit.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    public static void setRecord(Context context, int record) {
        SharedPreferences sp = context.getSharedPreferences("Record", Context.MODE_PRIVATE);
        sp.edit().putInt("record", record).apply();
    }

    public static int getRecord(Context context) {
        SharedPreferences sp = context.getSharedPreferences("Record", Context.MODE_PRIVATE);
        return sp.getInt("record", 0);
    }


}
