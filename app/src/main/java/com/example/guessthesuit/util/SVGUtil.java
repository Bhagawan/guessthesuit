package com.example.guessthesuit.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;
import com.example.guessthesuit.data.Deck;

import java.io.IOException;

public class SVGUtil {

    public static void loadDeck() {
        loadCard("deck/Clubs/2C.svg", "2C");
        loadCard("deck/Clubs/3C.svg", "3C");
        loadCard("deck/Clubs/4C.svg", "4C");
        loadCard("deck/Clubs/5C.svg", "5C");
        loadCard("deck/Clubs/6C.svg", "6C");
        loadCard("deck/Clubs/7C.svg", "7C");
        loadCard("deck/Clubs/8C.svg", "8C");
        loadCard("deck/Clubs/9C.svg", "9C");
        loadCard("deck/Clubs/10C.svg", "10C");
        loadCard("deck/Clubs/AC.svg", "AC");
        loadCard("deck/Clubs/JC.svg", "JC");
        loadCard("deck/Clubs/KC.svg", "KC");
        loadCard("deck/Clubs/QC.svg", "QC");

        loadCard("deck/Diamonds/2D.svg", "2D");
        loadCard("deck/Diamonds/3D.svg", "3D");
        loadCard("deck/Diamonds/4D.svg", "4D");
        loadCard("deck/Diamonds/5D.svg", "5D");
        loadCard("deck/Diamonds/6D.svg", "6D");
        loadCard("deck/Diamonds/7D.svg", "7D");
        loadCard("deck/Diamonds/8D.svg", "8D");
        loadCard("deck/Diamonds/9D.svg", "9D");
        loadCard("deck/Diamonds/10D.svg", "10D");
        loadCard("deck/Diamonds/AD.svg", "AD");
        loadCard("deck/Diamonds/JD.svg", "JD");
        loadCard("deck/Diamonds/KD.svg", "KD");
        loadCard("deck/Diamonds/QD.svg", "QD");

        loadCard("deck/Hearts/2H.svg", "2H");
        loadCard("deck/Hearts/3H.svg", "3H");
        loadCard("deck/Hearts/4H.svg", "4H");
        loadCard("deck/Hearts/5H.svg", "5H");
        loadCard("deck/Hearts/6H.svg", "6H");
        loadCard("deck/Hearts/7H.svg", "7H");
        loadCard("deck/Hearts/8H.svg", "8H");
        loadCard("deck/Hearts/9H.svg", "9H");
        loadCard("deck/Hearts/10H.svg", "10H");
        loadCard("deck/Hearts/AH.svg", "AH");
        loadCard("deck/Hearts/JH.svg", "JH");
        loadCard("deck/Hearts/KH.svg", "KH");
        loadCard("deck/Hearts/QH.svg", "QH");

        loadCard("deck/Spades/2S.svg", "2S");
        loadCard("deck/Spades/3S.svg", "3S");
        loadCard("deck/Spades/4S.svg", "4S");
        loadCard("deck/Spades/5S.svg", "5S");
        loadCard("deck/Spades/6S.svg", "6S");
        loadCard("deck/Spades/7S.svg", "7S");
        loadCard("deck/Spades/8S.svg", "8S");
        loadCard("deck/Spades/9S.svg", "9S");
        loadCard("deck/Spades/10S.svg", "10S");
        loadCard("deck/Spades/AS.svg", "AS");
        loadCard("deck/Spades/JS.svg", "JS");
        loadCard("deck/Spades/KS.svg", "KS");
        loadCard("deck/Spades/QS.svg", "QS");
    }

    private static void loadCard(String filename, String id) {
        Deck.addCard(filename, id);
    }

    public static Bitmap getCard(Context context, String id) {
        try {
            SVG svg = SVG.getFromAsset(context.getAssets(), Deck.getCard(id));
            if (svg.getDocumentWidth() != -1) {
                Bitmap bitmap = Bitmap.createBitmap((int) Math.ceil(svg.getDocumentWidth()),
                        (int) Math.ceil(svg.getDocumentHeight()),
                        Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bitmap);
                svg.renderToCanvas(c);
                return  bitmap;

            }

        } catch (SVGParseException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
