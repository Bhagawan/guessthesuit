package com.example.guessthesuit.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Deck {
    private static final Map<String, String> deck = new HashMap<>();

    public static void addCard(String card, String id) {
        deck.put(id, card);
    }

    public static String getCard(String id) {
        return Deck.deck.get(id);
    }

    public static String getRandom() {
        Collection<String> cards = deck.keySet();
        int n = (int) (Math.random() * cards.size());
        for(String s : cards) if(--n < 0) return s;
        throw new AssertionError();
    }
}


