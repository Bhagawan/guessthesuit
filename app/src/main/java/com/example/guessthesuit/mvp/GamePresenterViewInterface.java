package com.example.guessthesuit.mvp;

import android.graphics.Bitmap;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.OneExecution;
import moxy.viewstate.strategy.alias.SingleState;

public interface GamePresenterViewInterface extends MvpView {

    @SingleState
    void flipCard(Bitmap card);

    @SingleState
    void setScore(int score);

    @OneExecution
    void showNotification(boolean success);

    @OneExecution
    void hideCard();
}
