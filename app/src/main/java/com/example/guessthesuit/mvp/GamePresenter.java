package com.example.guessthesuit.mvp;

import android.content.Context;
import android.os.Handler;

import com.example.guessthesuit.data.Deck;
import com.example.guessthesuit.util.SVGUtil;
import com.example.guessthesuit.util.SharedPref;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class GamePresenter extends MvpPresenter<GamePresenterViewInterface> {
    public static final byte CLUBS = 0;
    public static final byte DIAMONDS = 1;
    public static final byte HEARTS = 2;
    public static final byte SPADES = 3;
    private int score = 0;
    private int chain = 0;
    private boolean gameOn = true;

    @Override
    protected void onFirstViewAttach() {
        SVGUtil.loadDeck();
        getViewState().setScore(score);
    }

    public void performClick(Context context, byte skin) {
        if(gameOn) {
            String s = Deck.getRandom();
            getViewState().flipCard(SVGUtil.getCard(context, s));
            gameOn = false;
            char c = s.charAt(s.length()-1);
            if((skin == SPADES && c == 'S') ||
                    (skin == DIAMONDS && c == 'D') ||
                    (skin == CLUBS && c == 'C') ||
                    (skin == HEARTS && c == 'H')) success();
            else failure(context);
            Handler h = new Handler();
            h.postDelayed(() -> {
                getViewState().hideCard();
                gameOn = true;
            }, 3000);
        }
    }

    private void success() {
        chain++;
        score += 10 * chain;
        getViewState().showNotification(true);
        getViewState().setScore(score);

    }

    private void failure(Context context) {
        if(SharedPref.getRecord(context) < score) {
            SharedPref.setRecord(context, score);
        }
        chain = 0;
        score  = 0;
        getViewState().showNotification(false);
        getViewState().setScore(score);
    }

    public void saveScore(Context context) {
        if(SharedPref.getRecord(context) < score) {
            SharedPref.setRecord(context, score);
        }
    }

}
