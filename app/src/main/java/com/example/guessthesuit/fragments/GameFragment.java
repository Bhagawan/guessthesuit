package com.example.guessthesuit.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.guessthesuit.R;
import com.example.guessthesuit.mvp.GamePresenter;
import com.example.guessthesuit.mvp.GamePresenterViewInterface;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class GameFragment extends MvpAppCompatFragment implements GamePresenterViewInterface {
    private View mView;

    @InjectPresenter
    GamePresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_game, container, false);

        FrameLayout layoutClubs = mView.findViewById(R.id.image_game_clubs);
        layoutClubs.setOnClickListener(view -> mPresenter.performClick(getContext(), GamePresenter.CLUBS));
        FrameLayout layoutDiamonds = mView.findViewById(R.id.image_game_diamonds);
        layoutDiamonds.setOnClickListener(view -> mPresenter.performClick(getContext(), GamePresenter.DIAMONDS));
        FrameLayout layoutHearts = mView.findViewById(R.id.image_game_hearts);
        layoutHearts.setOnClickListener(view -> mPresenter.performClick(getContext(), GamePresenter.HEARTS));
        FrameLayout layoutSpades = mView.findViewById(R.id.image_game_spades);
        layoutSpades.setOnClickListener(view -> mPresenter.performClick(getContext(), GamePresenter.SPADES));
        return mView;
    }


    @Override
    public void onStop() {
        mPresenter.saveScore(getContext());
        super.onStop();
    }

    @Override
    public void flipCard(Bitmap card) {
        ImageView randomCard = mView.findViewById(R.id.image_game_random_card);
        randomCard.animate().withLayer()
                .rotationY(90)
                .setDuration(300)
                .withEndAction(
                        () -> {
                            randomCard.setImageBitmap(card);
                            randomCard.setRotationY(-90);
                            randomCard.animate().withLayer()
                                    .rotationY(0)
                                    .setDuration(300)
                                    .start();
                        }
                ).start();
    }

    @Override
    public void setScore(int score) {
        TextView scoreView = mView.findViewById(R.id.text_game_score);
        scoreView.setText(String.valueOf(score));
    }

    @Override
    public void showNotification(boolean success) {
        TextView header = mView.findViewById(R.id.textView_game_header);
        if(success) {
            header.setText(getString(R.string.string_win));
            header.setTextColor(ResourcesCompat.getColor(getResources(), R.color.notification_text_success, null));
        } else {
            header.setText(getString(R.string.string_lose));
            header.setTextColor(ResourcesCompat.getColor(getResources(), R.color.notification_text_failure, null));
        }
    }

    @Override
    public void hideCard() {
        ImageView randomCard = mView.findViewById(R.id.image_game_random_card);
        TextView header = mView.findViewById(R.id.textView_game_header);

        header.setText(getString(R.string.string_guess_skin));
        header.setTextColor(ResourcesCompat.getColor(getResources(), R.color.game_header, null));

        randomCard.animate().withLayer()
                .rotationY(90)
                .setDuration(300)
                .withEndAction(
                        () -> {
                            randomCard.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_bs_red_back, null));
                            randomCard.setRotationY(-90);
                            randomCard.animate().withLayer()
                                    .rotationY(0)
                                    .setDuration(300)
                                    .start();
                        }
                ).start();
    }
}