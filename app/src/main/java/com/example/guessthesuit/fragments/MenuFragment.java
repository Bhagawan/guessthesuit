package com.example.guessthesuit.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.guessthesuit.R;
import com.example.guessthesuit.util.SharedPref;

public class MenuFragment extends Fragment {
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_menu, container, false);

        androidx.appcompat.widget.AppCompatButton newGameButton = mView.findViewById(R.id.btn_menu_new_game);
        newGameButton.setOnClickListener(view -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainerView, new GameFragment())
                .addToBackStack(null)
                .commit());

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        TextView record = mView.findViewById(R.id.text_menu_score);
        record.setText(String.valueOf(SharedPref.getRecord(requireContext())));
    }
}